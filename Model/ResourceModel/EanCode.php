<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 10.12.2019
 * Time: 13:07
 */

namespace BrainIndustries\Ean\Model\ResourceModel;

/**
 * Class EanCode
 * @package BrainIndustries\Ean\Model\ResourceModel
 */
class EanCode extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * EanCode constructor.
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    )
    {
        parent::__construct($context);
    }

    /**
     *
     */
    protected function _construct()
    {
        $this->_init('brainindustries_ean_code', 'id');
    }

}
