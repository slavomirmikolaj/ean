<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 10.12.2019
 * Time: 13:39
 */

namespace BrainIndustries\Ean\Model\ResourceModel\EanCode;
use BrainIndustries\Ean\Model\EanCode;

/**
 * Class Collection
 * @package BrainIndustries\Ean\Model\ResourceModel\EanCode
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /** @var string */
    protected $_idFieldName = 'id';

    /** @var string */
    protected $_eventPrefix = 'brainindustries_ean_code_collection';

    /** @var string */
    protected $_eventObject = 'post_collection';

    /**
     *
     */
    protected function _construct()
    {
        $this->_init('BrainIndustries\Ean\Model\EanCode', 'BrainIndustries\Ean\Model\ResourceModel\EanCode');
    }

    /**
     * @return EanCode|null
     */
    public function getNonUsedCode()
    {
        $collection = $this->addFieldToFilter('assigned', ['eq' => false])
            ->setCurPage(0)
            ->setPageSize(1);

        if ($collection->getSize()) {
           return $collection->getFirstItem();
        }
        return null;
    }

}
