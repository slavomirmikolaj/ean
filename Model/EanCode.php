<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 09.12.2019
 * Time: 14:25
 */

namespace BrainIndustries\Ean\Model;

/**
 * Class EanCode
 * @package BrainIndustries\Ean\Model
 */
class EanCode extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{

    /** @var string */
    const CACHE_TAG = 'brainindustries_ean_code';

    /** @var string */
    protected $_cacheTag = 'brainindustries_ean_code';

    /** @var string */
    protected $_eventPrefix = 'brainindustries_ean_code';

    /**
     *
     */
    protected function _construct()
    {
        $this->_init('BrainIndustries\Ean\Model\ResourceModel\EanCode');
    }

    /**
     * @return array|string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @return array
     */
    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }

}
