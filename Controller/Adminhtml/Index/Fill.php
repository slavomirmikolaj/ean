<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 03.12.2019
 * Time: 9:25
 */

namespace BrainIndustries\Ean\Controller\Adminhtml\Index;
use Magento\Backend\App\Action;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use \Magento\Framework\Controller\Result\JsonFactory;

/**
 * Class Fill
 * @package BrainIndustries\Ean\Controller\Adminhtml\Index
 */
class Fill extends \Magento\Backend\App\Action
{

    /**
     * @var JsonFactory
     */
    private $jsonResultFactory;

    /**
     * @var \BrainIndustries\Ean\Model\EanCodeFactory
     */
    private $eanCodeFactory;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;


    public function __construct(
        Action\Context $context,
        JsonFactory $jsonResultFactory,
        \BrainIndustries\Ean\Model\EanCodeFactory $eanCodeFactory,
        ProductRepositoryInterface $productRepository
    ) {
        parent::__construct($context);
        $this->jsonResultFactory = $jsonResultFactory;
        $this->eanCodeFactory = $eanCodeFactory;
        $this->productRepository = $productRepository;
    }

    public function execute()
    {
        $eanCodeFactory = $this->eanCodeFactory->create();

        $confProduct = $this->productRepository->get(
            $this->getRequest()->getParam('sku')
        );

        $subProducts = $confProduct->getTypeInstance()->getUsedProducts($confProduct);
        $nonAssigned = [];
        foreach($subProducts as $childProduct)
        {
            if($childProduct->getEan() == null)
            {
                $nonAssigned[] = $childProduct;
            }
        }

        $codes = [];
        foreach($nonAssigned as $childProduct)
        {
            $code = $eanCodeFactory->getCollection()->getNonUsedCode();
            if ($code == null) {
                $codes[$childProduct->getId()] = NULL;
               continue;
            }

            $product = $this->productRepository->getById($childProduct->getId());
            $product->setData('ean', $code->getCode());
            $this->productRepository->save($product);

            $codes[$childProduct->getId()] = $code->getCode();
            $code->setAssigned(TRUE);
            $code->save();
        }

        $data = [
            'codes' => $codes
        ];

        $result = $this->jsonResultFactory->create();
        $result->setData($data);
        return $result;
    }

}
