<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 09.12.2019
 * Time: 13:37
 */

namespace BrainIndustries\Ean\Setup;

/**
 * Class InstallSchema
 * @package BrainIndustries\Ean\Setup
 */
class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{

    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function install(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        if(!$installer->tableExists('brainindustries_ean_code'))
        {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('brainindustries_ean_code')
            )->addColumn(
                    'id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary'  => true,
                        'unsigned' => true,
                    ],
                    'ID'
                )
                ->addColumn(
                    'code',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    128,
                    ['nullable' => false],
                    'Code'
                )
                ->addColumn(
                    'assigned',
                    \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                    null,
                    ['nullable' => false],
                    'Assigned'
                )
                ->setComment('BrainIndustries Ean Codes');
            $installer->getConnection()->createTable($table);
        }
        $installer->endSetup();
    }

}
