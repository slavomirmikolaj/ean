<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 19.11.2019
 * Time: 12:47
 */

namespace BrainIndustries\Ean\Plugin\Product;
use Magento\ConfigurableProduct\Ui\DataProvider\Product\Form\Modifier\ConfigurablePanel;
use Magento\Framework\UrlInterface;

/**
 * Class ConfigurablePanelPlugin
 * @package BrainIndustries\Ean\Plugin\Product
 */
class ConfigurablePanelPlugin
{

    /**
     * @var UrlInterface
     */
    private $urlBuilder;

    public function __construct(
        UrlInterface $urlBuilder
    ) {
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * @param ConfigurablePanel $subject
     * @param array $meta
     * @return array
     */
    public function afterModifyMeta(ConfigurablePanel $subject, array &$meta)
    {
        $eanColumn = $this->getTextColumn(
            'ean',
            __('EAN')
        );

        /*$checkboxColumn = $this->getCheckboxColumn(
            'checked',
            __('Select')
        );

        $meta[ConfigurablePanel::GROUP_CONFIGURABLE]['children']['configurable-matrix']['children']['record']['children'] = [
            'checked' => $checkboxColumn
            ] + $meta[ConfigurablePanel::GROUP_CONFIGURABLE]['children']['configurable-matrix']['children']['record']['children'];*/


        $meta[ConfigurablePanel::GROUP_CONFIGURABLE]['children']['configurable-matrix']['children']['record']['children'] = $this->arrayInsertAfter(
            'attributes',
            $meta[ConfigurablePanel::GROUP_CONFIGURABLE]['children']['configurable-matrix']['children']['record']['children'],
            'ean',
            $eanColumn
        );

        $meta[ConfigurablePanel::GROUP_CONFIGURABLE]['children']['configurable_products_button_set']['children'] = $this->arrayInsertAfter(
            'create_configurable_products_button',
            $meta[ConfigurablePanel::GROUP_CONFIGURABLE]['children']['configurable_products_button_set']['children'],
            'fill_ean_button',
            [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'formElement' => 'container',
                            'componentType' => 'container',
                            'component' => 'BrainIndustries_Ean/js/form/components/fill-ean-button',
                            'title' => __('Fill EAN Codes'),
                            'sortOrder' => 15,
                            'reqUrl' => $this->urlBuilder->getUrl('brainindustries_ean/index/fill')
                        ],
                    ],
                ],
            ]
        );

        return $meta;
    }

    /**
     * @param $name
     * @param \Magento\Framework\Phrase $label
     * @param array $editConfig
     * @param array $textConfig
     * @return mixed
     */
    private function getTextColumn(
        $name,
        \Magento\Framework\Phrase $label,
        $editConfig = [],
        $textConfig = []
    ) {
        $fieldEdit['arguments']['data']['config'] = [
            'dataType' => \Magento\Ui\Component\Form\Element\DataType\Number::NAME,
            'formElement' => \Magento\Ui\Component\Form\Element\Input::NAME,
            'componentType' => \Magento\Ui\Component\Form\Field::NAME,
            'dataScope' => $name,
            'fit' => true,
            'visibleIfCanEdit' => true,
            'imports' => [
                'visible' => '${$.provider}:${$.parentScope}.canEdit'
            ],
        ];
        $fieldText['arguments']['data']['config'] = [
            'componentType' => \Magento\Ui\Component\Form\Field::NAME,
            'formElement' => \Magento\Ui\Component\Form\Element\Input::NAME,
            'elementTmpl' => 'Magento_ConfigurableProduct/components/cell-html',
            'dataType' => \Magento\Ui\Component\Form\Element\DataType\Text::NAME,
            'dataScope' => $name,
            'visibleIfCanEdit' => false,
            'imports' => [
                'visible' => '!${$.provider}:${$.parentScope}.canEdit'
            ],
        ];
        $fieldEdit['arguments']['data']['config'] = array_replace_recursive(
            $fieldEdit['arguments']['data']['config'],
            $editConfig
        );
        $fieldText['arguments']['data']['config'] = array_replace_recursive(
            $fieldText['arguments']['data']['config'],
            $textConfig
        );
        $container['arguments']['data']['config'] = [
            'componentType' => \Magento\Ui\Component\Container::NAME,
            'formElement' => \Magento\Ui\Component\Container::NAME,
            'component' => 'Magento_Ui/js/form/components/group',
            'label' => $label,
            'dataScope' => '',
        ];
        $container['children'] = [
            $name . '_edit' => $fieldEdit,
            $name . '_text' => $fieldText,
        ];

        return $container;
    }

    private function getCheckboxColumn(
        $name,
        \Magento\Framework\Phrase $label
    ) {
        $fieldEdit['arguments']['data']['config'] = [
            'dataType' => \Magento\Ui\Component\Form\Element\DataType\Boolean::NAME,
            'formElement' => \Magento\Ui\Component\Form\Element\Checkbox::NAME,
            'componentType' => \Magento\Ui\Component\Form\Element\Checkbox::NAME,
            'fit' => false,
            'dataScope' => null,
            'visibleIfCanEdit' => true,
            'imports' => [
                'visible' => '${$.provider}:${$.parentScope}.canEdit'
            ],
        ];
        $fieldEdit['arguments']['data']['config'] = array_replace_recursive(
            $fieldEdit['arguments']['data']['config'],
            []
        );
        $container['arguments']['data']['config'] = [
            'componentType' => \Magento\Ui\Component\Container::NAME,
            'formElement' => \Magento\Ui\Component\Container::NAME,
            'component' => 'Magento_Ui/js/form/components/group',
            'label' => $label,
            'dataScope' => null,
        ];
        $container['children'] = [
            $name . '_edit' => $fieldEdit
        ];

        return $container;
    }

    /**
     * @param $key
     * @param array $array
     * @param $new_key
     * @param $new_value
     * @return array|bool
     */
    private function arrayInsertAfter($key, array &$array, $new_key, $new_value)
    {
        if (array_key_exists($key, $array)) {
            $new = array();
            foreach ($array as $k => $value) {
                $new[$k] = $value;
                if ($k === $key) {
                    $new[$new_key] = $new_value;
                }
            }
            return $new;
        }
        return FALSE;
    }

}
