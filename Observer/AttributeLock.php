<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 19.11.2019
 * Time: 14:01
 */

namespace BrainIndustries\Ean\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class AttributeLock
 * @package BrainIndustries\Ean\Observer
 */
class AttributeLock implements ObserverInterface
{

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $product = $observer->getEvent()->getProduct();
        $product->lockAttribute('ean');
    }

}
